#include <stdlib.h>
#include "abr.h"

/* 
 * bool recherche_abr (double d, struct noeud* A0)
 * {
 *     if (A0 == NIL)
 *         return false;
 *     else if (A0->valeur == d)
 *         return true;
 *     else if (A0->valeur < d)
 *         return recherche_abr (d, A0->droit);
 *     else
 *         return recherche_abr (d, A0->gauche);
 * }
 */

bool recherche_abr (double d, struct noeud* A0)
{   struct noeud* A;
    bool found;

    found = false; /* on n'a pas - encore - trouvé d */
    A = A0;
    while (A != NIL && ! found)
    {   if (A->valeur == d)
            found = true;
        else if (A->valeur < d) /* d est + grand - on va à droite */
            A = A->droit;
        else
            A = A->gauche;
    }
    return found;
}

/* Fonction auxiliaire non exportée */

static struct noeud* new_feuille (double d)
{   struct noeud* F;

    F = malloc (sizeof(struct noeud));
    F->gauche = NIL;
    F->valeur = d;
    F->droit = NIL;
    return F;
}

struct noeud* ajout_abr (struct noeud* A0, double d)
{
    if (A0 == NIL)
        return new_feuille (d); 
                    /* si A0 est vide le nouvel arbre est une feuille */
    else
    {   struct noeud* prec;
        struct noeud* cour;
        cour = A0;
        while (cour != NIL)
        {   prec = cour;
            if (cour->valeur < d)
                cour = cour->droit;
            else /* on n'est pas censé avoir égalité et donc valeur > d */
                cour = cour->gauche;
        }
/* On s'arrête avec cour == NIL et prec == le struct noeud à modifier */
        if (prec->valeur < d)
            prec->droit = new_feuille (d);
        else
            prec->gauche = new_feuille (d);

        return A0;  /* l'ajout de d n'a pas modifié l'ancienne racine */
    }
}


