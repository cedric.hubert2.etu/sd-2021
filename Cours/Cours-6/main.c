/* main.c */

#include <stdio.h>
#include "abr.h"

int main ()
{   double T [] = { 17, 34, 21, 48, 109, 5 };
    int n = sizeof(T)/sizeof(double);

    struct noeud* A0;

/* 
 * Pas de constructeur parce que A0 n'est pas une structure
 * Et donc on n'a pas beaucoup de possibilités pour l'initialisation
 */
    A0 = NIL;
    for (int i = 0; i < n; i++)
    {
        A0 = ajout_abr (A0, T[i]);
    }
    if (recherche_abr (48, A0))
        printf ("48 est dans l'ABR\n");
    else
        printf ("48 n'est pas dans l'ABR\n");
/* On va avoir une fuite mémoire */
    return 0;
}

