/* abr.h */

struct noeud {
    struct noeud* gauche;
    double valeur;
    struct noeud* droit;
};

#define NIL (struct noeud*)0

/* Un ABR est un struct noeud* */

/* Ajoute d à l'ABR A et retourne la racine de l'ABR A modifié */
extern struct noeud* ajout_abr (struct noeud* A, double d);
/*     ------------- ---------
       type retourné   nom
 */

#include <stdbool.h>

/* Retourne true si d apprtient à A et false sinon */
extern bool recherche_abr (double d, struct noeud* A);


