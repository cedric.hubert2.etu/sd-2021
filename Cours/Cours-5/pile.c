/* pile.c */

#include <assert.h>
#include "pile.h"

void init_pile (struct pile* P)
{
    P->sp = 0;
}

void clear_pile (struct pile* P)
{
}

/* Empile d dans P */
void empiler (struct pile* P, double d)
{
    assert (P->sp < DMAX-1);
    P->T [P->sp] = d;
    P->sp += 1;
/*
 * Ou encore :
 *  P->T [P->sp++] = d;
 */
}
    
/* Dépile un élément de P et l'affecte à d. */
void depiler (double* d, struct pile* P)
{
    assert (! pile_vide (P));
    P->sp -= 1;
    *d = P->T [P->sp];
/*
 * Ou encore :
 *  *d = P->T [--P->sp];
 */
}

/* Retourne true si la pile est vide */
bool pile_vide (struct pile* P)
{
    return P->sp == 0;
}

