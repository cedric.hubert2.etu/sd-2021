/* main.c */

#include "rationnel.h"

int main ()
{
    struct rationnel A, B;

    init_rationnel (&A, 2, 4);
    init_rationnel (&B, -3, -8);
/* A = A + B */
    add_rationnel (&A, &A, &B);
    print_rationnel (A);
    clear_rationnel (&A);
    clear_rationnel (&B);
    return 0;
}

