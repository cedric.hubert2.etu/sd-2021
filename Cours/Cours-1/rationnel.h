/* rationnel.h */

/* DECLARATION DU TYPE */

struct rationnel {
    int numer;
    int denom;
};

/* SPECIFICATIONS DU TYPE

   Le type struct rationnel implante des nombres rationnels.
   Le champ numer contient le numérateur
   Le champ denom contient le dénominateur
   Il est strictement positif (le signe est porté par le numérateur)
   La fraction est réduite : le pgcd du numérateur et du dénominateur vaut 1
*/

/* Constructeur de struct rationnel. Affecte à R (mode R) le rationnel p/q.
   Le dénominateur q est censé être non nul. p et q en mode D.
 */

extern void init_rationnel (struct rationnel* R, int p, int q);

/* Destructeur de struct rationnel */

extern void clear_rationnel (struct rationnel*);

/* Affecte à S le rationnel A + B.
   S est en mode R. A et B sont en mode D.
 */

extern void add_rationnel 
    (struct rationnel* S, struct rationnel* A, struct rationnel* B);

/* Affiche le rationnel passé en paramètre sur la sortie standard */

extern void print_rationnel (struct rationnel);



