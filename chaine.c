/* chaine.c */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine.h" //On inclut les fonctions de chaine.h

/*
 * Constructeur
 *      On passe struct chaine en mode résultat
 *      On récupère le nombre de caractères du mot saisi
 *      et on effectue un malloc de la taille correspondante pour construire mot
 */

void Init_chaine(struct chaine* mot) {
    Init_liste(&mot->Liste);
}
        
void Free_chaine(struct chaine* mot) {
    Clear_liste(&mot->Liste);
}

void Ajout_carac(struct chaine* mot, char c) {
    Ajout_en_queue(&mot->Liste, c);
}

void Affichage_chaine(struct chaine* mot) {
    Print_liste(&mot->Liste);
}


    
