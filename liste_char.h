/* liste.h */

/* Struct maillon */

struct maillon {
    char carac;
    struct maillon* suivant;
};

/* Struct Liste */

struct liste_char {
    struct maillon* tete;
    int nbElem;
};

/* Pointeur nul de type maillon */

#define NIL (struct maillon*)0


/* Prototypes de fonctions */

// Constructeur
extern void Init_liste(struct liste_char*);

// Destructeur
extern void Clear_liste(struct liste_char*);

// Ajout en tete
extern void Ajout_en_tete(struct liste_char*,char);

// Ajout en queue
extern void Ajout_en_queue(struct liste_char*, char);

// Affichage
extern void Print_liste(struct liste_char*);
