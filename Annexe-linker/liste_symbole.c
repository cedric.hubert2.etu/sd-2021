#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "liste_symbole.h"
#include "error.h"

/**
 * @file
 * @brief Implantation des listes de symboles
 * @author F. Boulier
 * @date février 2011
 */

/**
 * @brief Constructeur. 
 * @param[out] L une liste
 */

void init_liste_symbole (struct liste_symbole* L)
{
    L->tete = (struct maillon_symbole*)0;
    L->nbelem = 0;
}

/**
 * @brief Destructeur.
 * @param[out] L une liste
 */

void clear_liste_symbole (struct liste_symbole* L)
{   struct maillon_symbole* courant;
    struct maillon_symbole* suivant;
    int i;

    courant = L->tete;
    for (i = 0; i < L->nbelem; i++)
    {   suivant = courant->next; /* on consulte courant->next avant */
	clear_symbole (&courant->value);
        free (courant);          /* d'exécuter l'instruction free ! */
        courant = suivant;
    }
}

/**
 * @brief Ajoute une copie de B en tête de L.
 * @param[in,out] L une liste
 * @param[in] B un symbole
 */

void ajouter_en_tete_liste_symbole (struct liste_symbole* L, struct symbole* B)
{   struct maillon_symbole* nouveau;

    nouveau = (struct maillon_symbole*)malloc (sizeof (struct maillon_symbole));
    if (nouveau == (struct maillon_symbole*)0)
        error ("ajouter_en_tete_liste_symbole", __FILE__, __LINE__);

    init_symbole (&nouveau->value);
    set_symbole (&nouveau->value, B);
    nouveau->next = L->tete;

    L->tete = nouveau;
    L->nbelem += 1;
}

/**
 * @brief Retourne \p true si L est vide, \p false sinon
 * @param[in] L une liste
 */

bool est_vide_liste_symbole (struct liste_symbole* L)
{
    return L->nbelem == 0;
}

/**
 * @brief Cherche s'il existe un symbole d'identificateur \p clef
 * dans la liste L. Si oui, affecte l'adresse du symbole à \p *sym,
 * sinon affecte le pointeur nul à \p *sym. 
 * Retourne le nombre de comparaisons effectuées par la fonction.
 * @param[out] sym un pointeur sur symbole
 * @param[in] clef une chaîne de caractères
 * @param[in] L une liste
 */

int rechercher_dans_liste_symbole 
		(struct symbole** sym, char* clef, struct liste_symbole* L)
{
    struct maillon_symbole* M;
    bool found;
    int cpt;

    found = false;
    M = L->tete;
    cpt = 0;
    while (M && !found)
    {	cpt += 1;
	if (strcmp (M->value.ident, clef) == 0)
	    found = true;
	else
	    M = M->next;
    }
    if (found)
	*sym = &M->value;
    else
	*sym = (struct symbole*)0;
    return cpt;
}

/**
 * @brief Imprime L.
 * @param[in] L une liste
 */

void imprimer_liste_symbole (struct liste_symbole* L)
{   struct maillon_symbole* M;
    int i;

    printf ("[");
    M = L->tete;
    for (i = 0; i < L->nbelem; i++)
    {   if (i != 0)
	    printf (", ");
        imprimer_symbole (&M->value);
        M = M->next;
    }
    printf ("]\n");
}

