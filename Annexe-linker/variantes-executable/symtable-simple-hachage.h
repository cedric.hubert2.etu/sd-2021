#if ! defined (SYMTABLE_H)
#define SYMTABLE_H 1

#include <stdbool.h>
#include "symbole.h"
#include "table_simple_hachage.h"
#include "stats.h"

/**
 * @file
 * @brief Les types nécessaires à la manipulation de la table
 * des symboles d'un exécutable, c'est-à-dire la sortie du linker.
 * @author F. Boulier
 * @date mars 2011
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct symtable
 * @brief Le type \p struct \p symtable fournit une implantation très 
 * rudimentaire de la table des symboles d'un exécutable. Les
 * données sont stockées dans une table de hachage \p T avec résolution
 * des conflits par listes chaînées.
 */

struct symtable
{   struct table_simple_hachage T; /**< la table de hachage */
    struct stats mesures;          /**< mesures statistiques */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_symtable (struct symtable*);
extern void clear_symtable (struct symtable*);
extern void enregistrer_dans_symtable (struct symtable*, struct symbole*);
extern struct symbole* rechercher_dans_symtable (char*, struct symtable*);
extern int synthese_symtable (struct symtable*);

#endif
