#if ! defined (LISTE_SYMBOLE_H)
#define LISTE_SYMBOLE_H 1

#include "symbole.h"

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

struct maillon_symbole
{   struct symbole value;
    struct maillon_symbole* next;
};

struct liste_symbole
{   struct maillon_symbole* tete;
    int nbelem;
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_liste_symbole (struct liste_symbole*);
extern void clear_liste_symbole (struct liste_symbole*);
extern void imprimer_liste_symbole (struct liste_symbole*);
extern bool est_vide_liste_symbole (struct liste_symbole*);
extern void ajouter_en_tete_liste_symbole 
			(struct liste_symbole*, struct symbole*);
extern int rechercher_dans_liste_symbole 
			(struct symbole**, char*, struct liste_symbole*);

#endif
