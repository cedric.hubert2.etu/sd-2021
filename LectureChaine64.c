#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main () {
    int c,taille;
    char *s; 
    //On alloue à s la taille de 64 char
    s = (char*)malloc(64*sizeof(char)); 
    //On stocke les caractèresdans c
    c = getchar ();
    taille = 0;
    while (! isspace (c)) {
        //On stocke c dans s
        s[taille]=c;
        taille++;
        c = getchar (); 
    }
    //On affiche s
    for (int i=0; i<taille; i++) {
        putchar(s[i]);
    }
    putchar('\n');
    
    //On libère l'espace alloué à s 
    free(s);
    return 0;
}
