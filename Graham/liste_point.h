/* liste_point.h */

#include "point.h"

/* Structures */

struct maillon_point {
    struct point* point;
    struct maillon_point* suivant;
};

struct liste_point {
    struct maillon_point* tete;
    int nbelem;
};


/* Fonctions */

extern void init_liste_point (struct liste_point*);

extern void clear_liste_point (struct liste_point*);

extern void ajouter_en_tete_point (struct liste_point*, struct point*);

extern void extraire_tete_liste_point (struct point*, struct liste_point*);
