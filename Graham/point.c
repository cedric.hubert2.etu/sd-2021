/* point.c */

#include "point.h"
#include <stdbool.h>


/* Constructeur */

void init_point (struct point* P, double a, double b, char nom) {
    P->x = a;
    P->y = b;
    P->ident = nom;
}
    
/* Comparaison */

int compare_points (const void* p0, const void* q0) {
    struct point* q = (struct point*)q0;
    struct point* p = (struct point*)p0;
    double det = p->x * q->x - p->y * q->y;
    // det > 0 = sens trigo = sens de l'algo de graham
    // On renvoie -1,0 ou 1 pour trier par ordre croissant avec qsort
    if (det > 0.0) return -1;
    else if (det == 0.0) return 0;
    else return 1;
}

/* Tourne a gauche */

bool tourne_a_gauche (struct point* A, struct point* B, struct point* C) {
    double det = (B->x - A->x)*(C->y - A->y) - (B->y - A->y)*(C->x - A->x);
    // det > 0 = le chemin A-B-C tourne a gauche
    if (det > 0)
        return true;
    else 
        return false;
}
        
