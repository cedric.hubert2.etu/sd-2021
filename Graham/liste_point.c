/* liste_point.c */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "liste_point.h"

/* Constructeur */

void init_liste_point (struct liste_point* L) {
    L->tete = (struct maillon_point*)0;
    L->nbelem = 0;
}

/* Destructeur */

void clear_liste_point (struct liste_point* L) {
    struct maillon_point* M;
    struct maillon_point* temp;
    M = L->tete;
    for (int i = 0; i < L->nbelem; i++) {
        temp = M->suivant;
        free(M);
        M = temp;
    }
}

/* Empiler = Ajout d'un point en tete de liste */

void ajouter_en_tete_point (struct liste_point* L, struct point* P) {
    struct maillon_point* M;
    M = (struct maillon_point*)malloc (sizeof(struct maillon_point));
    M->point = P;
    M->suivant = L->tete;
    L->tete = M;
    L->nbelem += 1;
}

/* Depiler = Extraire le point en tete de liste */

void extraire_tete_liste_point (struct point* P, struct liste_point* L) {
    P = L->tete->point;
    L->tete = L->tete->suivant;
    L->nbelem = L->nbelem - 1;
}


    
