/* liste_char.h */

/* Spécifications : voir le dessin */

/* les maillons = structures "horizontales" dans le tas */

struct maillon_char
{
  char valeur;
  struct maillon_char *suivant;
};

/* le pointeur nul de type struct maillon* */

#define NIL (struct maillon_char*)0

/* les listes = structures "verticales" souvent dans la pile */
/* les variables locales seront de type struct liste_char */

struct liste_char
{
  struct maillon_char *tete;
  int nbelem;
};

extern void init_liste_char (struct liste_char *);
extern void ajout_en_tete_liste_char (struct liste_char *, char);
extern void print_liste_char (struct liste_char);
extern void clear_liste_char (struct liste_char *);
