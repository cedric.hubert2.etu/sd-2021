#include "chaine.h"


void init_chaine (struct chaine *c)
{
  init_liste_char (&c->L);
}

void clear_chaine (struct chaine *c)
{
  clear_liste_char (&c->L);
}

void ajout_en_queue_chaine_char (struct chaine *c, char a)
{
  ajout_en_tete_liste_char (&c->L, a);
}

void print_chaine (struct chaine *c)
{
  print_liste_char (c->L);
}
