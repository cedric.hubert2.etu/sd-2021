/* liste_char.c */

#include <stdio.h>
#include <stdlib.h>
#include "liste_char.h"

void init_liste_char (struct liste_char *L)
{
  L->tete = NIL;
  L->nbelem = 0;
}

void ajout_en_tete_liste_char (struct liste_char *L, char v)
{
  struct maillon_char *M;
  M = (struct maillon_char *) malloc (sizeof (struct maillon_char));
  M->valeur = v;
  M->suivant = L->tete;
  L->tete = M;
  L->nbelem += 1;
}

void print_liste_char (struct liste_char L)
{
  struct maillon_char *M;
  M = L.tete;
  for (int i = 0; i < L.nbelem; i++)
    {
      printf ("%c ", M->valeur);
      M = M->suivant;
    }
  printf ("\n");
/*
Variante:
    M = L.tete;
    while (M != NIL)
    {   printf ("%lf ", M->valeur);
        M = M->suivant;
    }
Autre variante:
    for (M = L.tete; M != NIL; M = M->suivant)
        printf ("%lf ", M->valeur);
 */
}

void clear_liste_char (struct liste_char *L)
{
/* Ici, on doit faire des free */
}
