/* liste_char.c */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "liste_char.h"

/* Constructeur */

void Init_liste(struct liste_char* L) {
    L->tete = NIL;
    L->nbElem = 0;
}
    
    
void Ajout_en_tete(struct liste_char* L, char c) {
    struct maillon* M;
    M = (struct maillon*)malloc (sizeof(struct maillon));
    M->carac = c;
    M->suivant = L->tete;
    L->tete = M;
    L->nbElem += 1;
}

void Ajout_en_queue(struct liste_char* L, char c) {
    struct maillon* queue, *M;
    // Cas ou la tete est vide
    if (L->tete == NIL) {
        Ajout_en_tete(L,c);
    } else {
        queue = L->tete;
        while (queue->suivant != NIL) {
            queue = queue->suivant;
        }
        M = (struct maillon*)malloc (sizeof(struct maillon*));
        M->carac = c;
        M->suivant = NIL;
        queue->suivant = M;
        L->nbElem += 1;
    }
}

void Print_liste(struct liste_char* L) {
    struct maillon* M;
    M = L->tete;
    for (int i = 0; i < L->nbElem; i++) {
        printf("%c",M->carac);
        M = M->suivant;
    }
    printf("\n");
}

void Clear_liste(struct liste_char* L) {
    struct maillon* M;
    struct maillon* temp;
    M = L->tete;
    for (int i = 0; i < L->nbElem; i++) {
        temp = M->suivant;
        free(M);
        M = temp;
    }
}
