/* chaine.h */

#include "liste_char.h"

/* DECLARATION D'UNE STRUCTURE */

struct chaine {
    struct liste_char Liste;
};

/* SPECIFICATIONS DE LA STRUCTURE 
 * 
 * Une chaine comporte une zone 
 * zone est un pointeur de type caractère pointant vers une zone allouée dynamiquement
 * 
 */
//Constructeur de struct chaine
extern void Init_chaine(struct chaine* mot);

//Destructeur de struct chaine 
extern void Free_chaine(struct chaine* mot);

//Ajout d'un caractère c à la fin de la chaine
extern void Ajout_carac(struct chaine* mot, char c);

//Affichage de la chaine de caractères
extern void Affichage_chaine(struct chaine* mot);
