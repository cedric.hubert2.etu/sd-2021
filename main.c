/* main.c */

#include <stdio.h>
#include "chaine.h"

int main() {
    struct chaine s;
    
    char MOT[] = {'t','e','s','t'};
    
    Init_chaine(&s);
    
    for (int i = 0; i<4; i++) {
        char c = MOT[i];
        Ajout_carac(&s,c);
        printf("Votre nouveau mot : \n");
        Affichage_chaine(&s);
    }
    
    Free_chaine(&s);
    return 0;
}
    
    
