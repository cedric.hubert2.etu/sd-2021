/* abr.h */

/* Struct */

struct abr {
    struct abr* gauche;
    struct abr* droit;
    double valeur;
};

/* Constantes globales */

#define NIL (struct abr*)0

/* Fonctions */

// Ajout d'une nouvelle feuille
extern struct abr* ajouter_abr (double, struct abr*);

// Affichage de l'abr croissant
extern void afficher_abr_croissant (struct abr*);

// Calcul de la hauteur de l'abr 
extern int hauteur_abr (struct abr*); 

// Calcul du nombre de noeuds de l'abr 
extern int nombre_noeuds_abr (struct abr*);

// Destructeur 
extern void clear_abr (struct abr*);

// Fonction non recursive pour le fichier .aff_dot_rec
extern void aff_dot (struct abr*);

// Ecriture du fichier .dot
extern void aff_dot_rec (struct abr*);

// Affichage indente
extern void aff_indente (struct abr*);

// Recherche d'une valeur dans l'arbre (la fonction renvoie 1 si la valeur est dans l'arbre
extern int recherche_abr (struct abr*, int);




