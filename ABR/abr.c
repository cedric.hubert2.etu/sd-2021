/* abr.c */

#include "abr.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>

/* Fonction static qui reste dans le fichier .c */

static struct abr* new_feuille (double d) {
    struct abr* F;
    
    F = malloc (sizeof(struct abr));
    F->gauche = NIL;
    F->droit = NIL;
    F->valeur = d;
    return F;
}

struct abr* ajouter_abr (double d, struct abr* A0) {
    if (A0 == NIL)
        return new_feuille(d);
    else {
        struct abr* prec;
        struct abr* cour;
        cour = A0;
        while (cour != NIL) {
            prec = cour;
            if (cour->valeur < d)
                cour = cour->droit;
            else
                cour = cour->gauche;
        }
        if (prec->valeur < d)
            prec->droit = new_feuille (d);
        else 
            prec->gauche = new_feuille (d);
        
        return A0;
    }
}

void afficher_abr_croissant(struct abr* A0) {
    if (A0 != NIL) {
        afficher_abr_croissant(A0->gauche);
        printf("%lf\n",A0->valeur);
        afficher_abr_croissant(A0->droit);
    }
}

int hauteur_abr(struct abr* A0) {
    if (A0 != NIL) {
        int max;
        struct abr* cour;
        cour = A0;
        if (hauteur_abr(cour->gauche)>hauteur_abr(cour->droit)) {
            max = hauteur_abr(cour->gauche);
        } else { 
            max = hauteur_abr(cour->droit);
        }
        return (1 + max);
    } else {
        return 0;
    }
}
    
int nombre_noeuds_abr (struct abr* A0) {
    if (A0 != NIL) {
        struct abr* cour;
        cour = A0;
        return (1 + nombre_noeuds_abr(cour->gauche) + nombre_noeuds_abr(cour->droit));
    } else {
        return 0;
    }
}
    
void clear_abr (struct abr* A0) {
    if (A0 != NIL) {
        struct abr* cour;
        cour = A0;
        if (cour->gauche != NIL) {
            clear_abr(cour->gauche);
            cour->gauche = NIL;
        }
        if (cour->droit != NIL) {
            clear_abr(cour->droit);
            cour->droit = NIL;
        }
        free (A0);
    }
}
    
void aff_dot_rec (struct abr* A0) {
    FILE *dot = fopen("ABR.dot","a+");
    if (A0 != NIL) {
        if (A0->gauche != NIL) {
            fprintf(dot,"%lf -> %lf [label=\"gauche\"] \n", A0->valeur, A0->gauche->valeur);
            aff_dot_rec(A0->gauche);
        }
        if (A0->droit != NIL) {
            fprintf(dot,"%lf -> %lf [label=\"droit\"] \n", A0->valeur, A0->droit->valeur);
            aff_dot_rec(A0->droit);
        }
    }
    if ((A0->droit == NIL) && (A0->gauche == NIL)) {
        fprintf(dot,"%lf \n", A0->valeur);
    }   
    fclose(dot);
}

void aff_dot (struct abr* A0) {
    FILE *dot = fopen("ABR.dot","w");
    fprintf (dot, "digraph G {\n");
    fclose(dot);
    aff_dot_rec(A0);
    FILE *dot2 = fopen("ABR.dot","a+");
    fprintf (dot, "}");
    fclose(dot2);
}

/*
int recherche_abr (struct abr* A0, int val) {
    int result = 0;
    int i;
    while ((result != 1) && (i<hauteur_abr(A0))) {
        if (val == A0->valeur) result = 1; 
        if (val > A0->gauche->valeur) {
            A0 = A0->droit;
            i++;
        } else {
            A0 = A0->gauche;
            i++;
        }
    }
    return result;
}
*/
            
    

/*
void aff_indente (struct abr* A0) {
    if (A0 != NIL) {
        struct abr* cour;
        struct abr* prec;
        cour = A0;
        int i = 0;
        while (cour->gauche != NIL) {
            printf("%lf    ",cour->valeur);
            cour = cour->gauche;
            i++;
        }
        int iprec = i-1
        while ((cour->droit == NIL) && (niv < iprec)) {
            cour=cour->gauche;
        }
        while ((cour<    
        printf("\n");
        while (cour->droit != NIL) {
            printf("%lf    ",cour->droit->valeur);
            cour = cour->droit;
            niv++;
    }
}
*/

    
    
            
    


