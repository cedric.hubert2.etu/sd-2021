/* main.c */

#include <stdio.h>
#include "abr.h"

int main () {
    struct abr* racine;
    double x;
    racine = NIL;
    scanf ("%lf", &x);
    while (x != -1)
    {
        racine = ajouter_abr (x, racine);
        scanf ("%lf", &x);
    }
    printf ("Affichage des noeuds dans l'ordre croissant \n");
    afficher_abr_croissant(racine);
    aff_dot (racine);
    printf ("Fichier .dot cree ! \n");
    printf ("Recherche_abr : %d \n", recherche_abr(racine,3));
    printf ("la hauteur de l'ABR est %d \n", hauteur_abr (racine));
    printf ("le nombre de noeuds de l'ABR est %d \n", nombre_noeuds_abr (racine));
    clear_abr (racine);
    return 0;
}
