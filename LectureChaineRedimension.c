#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main () {
    int c,taille;
    char *s;
    //On introduit n qui va compter le nombre de fois ou l'on augmente la mémoire
    int n = 1;
    //On commence en allouant seulement 4 char à s
    s = (char*)malloc(4*sizeof(char)); 
    //On stocke les caractères dans c
    c = getchar ();
    taille = 0;
    while (! isspace (c)) {
        //On stocke c dans s
        s[taille]=c;
        // Si la taille max est atteinte, on rajoute 8 char dispo
        if (taille == sizeof(s)/sizeof(char)) {
            s = realloc(s,n*8*sizeof(char));
            n++;
        }
        taille++;
        c = getchar (); 
    }
    //On affiche s
    for (int i=0; i<taille; i++) {
        putchar(s[i]);
    }
    putchar('\n');
    
    //On libère l'espace alloué à s 
    free(s);
    return 0;
}
