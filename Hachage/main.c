/* main.c */

#include <stdio.h>
#include "hachage_simple.h"

int main () {
    struct table T;
    double x;
    
    init_table(&T);
    scanf("%lf", &x);
    while (x != -1) {
        enregistrer_table(&T, x);
        imprimer_table(&T);
        scanf("%lf", &x);
    }
    clear_table(&T);
    return 0;
}
