/* hachage_simple.c */

#include "hachage_simple.h"
#include "liste_double.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void init_table (struct table* T) {
    for (int i = 0; i<N; i++) {
        init_liste_double(&T->tab[i].L);
    }
}

void clear_table (struct table* T) {
    for (int i = 0; i<N; i++) {
        clear_liste_double(&T->tab[i].L);
    }
}

static int hachage (double val) {
    if (val%N <= 10) return ((int)val%N);
    else return (-1);
}

void enregister_table (struct table* T, double val) {
    if (hachage(val) != -1) {
        int n = hachage(val);
        ajouter_en_tete_liste_double(&T->tab[n].L, val);
    }
}

/* Pas fini */
bool rechercher_table (struct table* T, double val) {
    int n = hachage(val);
    if (n != -1) {
        bool trouve = false;
}
    
    
    
