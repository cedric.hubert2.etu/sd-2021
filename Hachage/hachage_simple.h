/* hachage_simple.h */

#include "liste_double.h"
#include <stdbool.h>

struct alveole {
    struct liste_double L;
};

#define N 10

struct table {
    struct alveole tab [N];
};

// constructeur de table de hachage
extern void init_table(struct table*);

// destructeur
extern void clear_table(struct table*);

// ajouter un nouveau double à la table
extern void enregistrer_table(struct table*, double);

// renvoie vrai si le double recherché est dans la table
extern bool rechercher_table(struct table*, double);

// affichage de la table
extern void imprimer_table(struct table*);
